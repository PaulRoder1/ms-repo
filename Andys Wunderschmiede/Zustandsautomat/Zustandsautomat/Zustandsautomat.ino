/*
 Name:    Zustandsautomat.ino
 Created: 28.11.2019 11:23:05
 Author:  Andy
*/

#include <SPI.h>
#include <MFRC522.h>
#include <LiquidCrystal.h>


#define LED_ROT   2    // Pin PD2
#define LED_GELB  3    // Pin PD3
#define LED_GRUEN 4    // Pin PD4

#define RST_PIN         9           // Configurable, see typical pin layout above
#define SS_PIN          10          // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

 int i = 1;


void thread_rot()
{
  static enum { AUS, EIN } zustand = AUS;
  static unsigned long int warteSeit = 0;

  switch (zustand)
  {
  case AUS:
    if (millis() - warteSeit >= 100)       // Bedingung: 100 ms später
    {
      digitalWrite(LED_ROT, HIGH);     // Reaktion: LED einschalten
      warteSeit = millis();
      zustand = EIN;                     // Nächster Zustand: EIN
    }
    break;

  case EIN:
    if (millis() - warteSeit >= 100)
    {
      digitalWrite(LED_ROT, LOW);
      warteSeit = millis();
      zustand = AUS;
    }
    break;
  }
}

void thread_gelb()
{
  static enum { AUS, EIN } zustand = AUS;
  static unsigned long int warteSeit = 0;

  switch (zustand)
  {
  case AUS:
    if (millis() - warteSeit >= 253)
    {
      digitalWrite(LED_GELB, HIGH);
      warteSeit = millis();
      zustand = EIN;
    }
    break;

  case EIN:
    if (millis() - warteSeit >= 253)
    {
      digitalWrite(LED_GELB, LOW);
      warteSeit = millis();
      zustand = AUS;
    }
    break;
  }
}

void thread_gruen()
{
  static enum { AUS, EIN } zustand = AUS;
  static unsigned long int warteSeit = 0;

  switch (zustand)
  {
  case AUS:
    if (millis() - warteSeit >= 378)
    {
      digitalWrite(LED_GRUEN, HIGH);
      warteSeit = millis();
      zustand = EIN;
    }
    break;

  case EIN:
    if (millis() - warteSeit >= 378)
    {
      digitalWrite(LED_GRUEN, LOW);
      warteSeit = millis();
      zustand = AUS;
    }
    break;
  }
}





unsigned long timer = 0;
bool NoContainer()
  {if (millis()>=0)
   { timer=millis();
    return true;
   }
   return false;
  }
// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(LED_ROT, OUTPUT);
  pinMode(LED_GELB, OUTPUT);
  pinMode(LED_GRUEN, OUTPUT);

  Serial.begin(9600);
  SPI.begin();
  mfrc522.PCD_Init();
  lcd.begin(16,2);
 
 
}

// the loop function runs over and over again until power down or reset
void loop() {
  switch (i)
  {
  case(1):    //Warten auf Kiste
    if (NoContainer)
    {
      thread_gruen;
      lcd.print("Bereit");
      
    }
    if (!NoContainer)
    {
      ++i;
    }
    break;

  case(2):    //Kiste Erkennen
    thread_gruen;
    if( mfrc522.PICC_IsNewCardPresent() )
      i+=2;

   ++i;
   break;

 case(3):     //Kiste unbekannt
    thread_gruen;
    thread_rot;
    lcd.print("Box einrichten o. herunternehmen");
    if (NoContainer)
      {
        i=1;
        break;
      }
    //Betriebsmodswechsel!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    break;

  case(4):    //Auftragsbearbeitung
      








  
  default:
    break;
  }
}
