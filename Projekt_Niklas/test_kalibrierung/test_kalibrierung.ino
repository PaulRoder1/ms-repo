#include <HX711.h> 

const int LOADCELL_DOUT_PIN = 4;
const int LOADCELL_SCK_PIN = 5;

HX711 scale;

char c;
long Tara; 
float Scale; 

void setup(){ 
  Serial.begin(9600); 
  delay(5000); 
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  Serial.println(F("Zur Kalibrierung der Stockwaage bitte den Anweisungen folgen!")); 
  Serial.println(); 
  Serial.println(F("Waage ohne Gewicht - Kalibrierung mit '1' und 'Enter' starten!")); 
  Serial.println();
 
  while (c != '1'){
    c = Serial.read();
   }; 
   
  c = 'x'; 
  Serial.println(F("Kalibriere ... ")); 
  Serial.println(); 
  scale.set_scale(); 
  scale.read_average(10); 
  Tara = scale.read_average(10); 
  Serial.println(Tara);
  Serial.println(F("Waage mit genau 1 kg belasten - Kalibrierung mit '2' und 'Enter' starten!"));
  Serial.println(); 
 
  while (c != '2') {
    c = Serial.read();
   };
  
  Serial.println(F("Kalibriere ... ")); 
  Serial.println();
  scale.set_offset(Tara); 
  scale.get_units(10); 
  Scale = ((scale.get_units(10)) / 1000); scale.set_scale(Scale); 
  Serial.print(F("Pruefe Gewicht: ")); 
  Serial.println(scale.get_units(10), 1);
  Serial.println(); 
  Serial.print(F("Taragewicht: ")); 
  Serial.println(Tara); 
  Serial.println(); 
  Serial.print(F("Skalierung: ")); 
  Serial.println(Scale); 
  Serial.println(); } 

void loop() {} 
