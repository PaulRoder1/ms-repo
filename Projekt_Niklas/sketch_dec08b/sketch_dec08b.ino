#include <RingBuf.h>
#include <HX711.h>

const int LOADCELL_DOUT_PIN = 4;
const int LOADCELL_SCK_PIN = 5;

HX711 scale;


void kalibrieren(){
  char c;
  int kGewicht = 1000;      //Gewicht das zur kalibbrierung verwendet wird
  int Anzahlm = 10;         //Anzahl der Messungen für den Avg-Wert
  long Tara; 
  float Scale; 

  Serial.begin(9600); 
  delay(200);
  Serial.println("Zur Kalibrierung der Stockwaage bitte den Anweisungen folgen!"); 
  Serial.println("Waage ohne Gewicht - Kalibrierung mit '1' und 'Enter' starten!");
 
  while (c != '1'){
    c = Serial.read();
   }; 
   
  c = 'x'; 
  Tara = scale.read_average(Anzahlm); 
  Serial.println("Kalibriere ... ");  
  scale.set_scale();
  Serial.println("Waage mit genau 1 kg belasten - Kalibrierung mit '2' und 'Enter' starten!"); 
 
  while (c != '2') {
    c = Serial.read();
   };
  
  Serial.println("Kalibriere ... "); 
  scale.set_offset(Tara);
  Scale = ((scale.get_value(Anzahlm)) / 1000);     
  scale.set_scale(Scale); 
  Serial.print("Pruefe Gewicht: "); 
  Serial.println(scale.get_units(Anzahlm/2) ,1);      //Anzahlm/2 auf Grund von performance 
  Serial.print("Taragewicht: "); 
  Serial.println(Tara); 
  Serial.print("Skalierung: "); 
  Serial.println(Scale);  
}


bool kiste_drauf(float gKiste){
  //if(scale.get_units() >= gKiste){            //500 nur als beispiel zu testen
  
  float Abweichung = 30.0;
  if(scale.get_units() >= Abweichung){
    return true;
  }
  else{ 
    return false;
  }
}


int herausgenommene_teile(float Bauteil_gewicht){
  float letztesGewicht = scale.get_units(5);
  float Gewicht = messen();
  float Differenz = letztesGewicht - Gewicht;
  int Anzahl_entnommeneTeile = Differenz/Bauteil_gewicht;
  return Anzahl_entnommeneTeile;
} 




int Gesamt_Anzahl(float gKiste, float gBauteil){
  float Gewicht_gesamt = messen();
  float Gewicht_bauteile = Gewicht_gesamt - gKiste;
  int Anzahl= Gewicht_bauteile / gBauteil;
  return Anzahl;
}


float messen(){
  float summen[2];
  float summeges = 0.0;
  for(int i = 0; i <=1 ; i++){
      summen[i] = scale.get_units(10);
      summeges += summen [i];
  }

  if(abs(summen[0]-summen[1]) < 1){
    return summeges/2;
  }

  else{
    messen();
  }
}


void setup(){
  
  Serial.begin(9600);
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  kalibrieren();
}


void loop(){

  float Gewicht_Kiste = 0;                           //zum testen kommt aus der datenbank
  float Gewicht_Bauteil = 5;                      //zum testen kommt aus der datenbank

  //bool Kiste = kiste_drauf(Gewicht_Kiste);
  //Serial.println(Kiste);
  
  int Anzahl_herausgenommen = herausgenommene_teile(Gewicht_Bauteil);
  Serial.println("herausgenommene Teile:  ");
  Serial.println(Anzahl_herausgenommen);
  
  int test = Gesamt_Anzahl(Gewicht_Kiste, Gewicht_Bauteil);
  Serial.println("Gesamt Anzahl: ");
  Serial.println(test);

  Serial.println();
 delay(5000);
}
